#include <iostream>
using namespace std;

int main() {
    int number;

    while(1) {

    cout << "Enter a five-digit integer (or -1 to quit): ";
    cin >> number;

    if (number == -1)
        break;

    int a,b,c,d;

    d = number % 10;
    c = (number % 100) / 10;
    b = (number % 10000) / 1000;
    a = (number % 100000) / 10000;

    cout << "The number " << number << " is ";

    if (a!=d || b!=c)
        cout << "not ";

    cout << "a palindrome." << endl;

    }
}
