#include <iostream>
using namespace std;

int main() {

    cout << "Enter the height of the pattern: ";
    int height;
    cin >> height;

    cout << "Your Figure with height " << height << " is displayed below!" << endl;


    for (int i = 0; i < height; i++) {
        for (int j = 0; j <= i; j++) {
            cout << "*";
        }
        cout << endl;
    }
}
