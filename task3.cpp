#include <iostream>
#include <cstring>
using namespace std;

int isPalindrome(string str) {
    int ret = 1;

    int head = 0, tail = str.length() - 1;

    while( head <= tail) {
        while (str[head] == ' ')
            head++;
        while (str[tail] == ' ')
            tail--;

        if (str[head] != str[tail]) {
            ret = 0;
            break;
        }

        head++;
        tail--;
    }

    return ret;
}

int main() {
    string str;

    while(1) {
        cout << "Enter a string (or -1 to quit): ";
        getline(cin, str);

        if (str == "-1")
            break;

        if (isPalindrome(str)) {
            cout << "The word \"" << str << "\" is a palindrome!" << endl;
        }
        else
            cout << "The word \"" << str << "\" is not a palindrome!" << endl;
    }

    cout << "Good bye!" << endl;
}
