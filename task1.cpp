#include <iostream>
using namespace std;

int main() {
    int account;
    double balance, charges, credits, limit;
    double newBalance;

    while(1) {

        cout << "Enter account number (or -1 to quit): ";
        cin >> account;

        if (account == -1)
            break;

        cout << "Enter beginning balance: ";
        cin >> balance;

        cout << "Enter total charges: ";
        cin >> charges;

        cout << "Enter total credits: ";
        cin >> credits;

        cout << "Enter credit limits: ";
        cin >> limit;

        newBalance = balance + charges - credits;

        cout << "New balance is " << newBalance << endl;

        if (newBalance > limit) {
            cout << "Account: " << account << endl;
            cout << "Credit limit: " << limit << endl;
            cout << "Balance: " << newBalance << endl;;
            cout << "Credit Limit Exceeded." << endl;
        }

        cout << endl;

    }

    cout << "Good bye!" << endl;
}